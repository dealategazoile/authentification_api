const router = require('express').Router()
const verify = require('../verify_token')

router.get('/' , verify , (req , res) =>{
    res.json({posts : {title : "post title" , body : "content of the post blabla blabla"}})

} )
router.get('/user' , verify , (req , res) =>{
    res.send(req.user)
} )

module.exports = router