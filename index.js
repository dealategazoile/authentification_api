const express = require('express')
const app = express()
const mongoose =  require('mongoose')
const dotenv = require('dotenv')
//import routes
const authRoute = require("./routes/auth") 
const postRoute = require("./routes/post") 

dotenv.config()

//Connect to DB
 mongoose.connect(process.env.DB_CONNECT, { useNewUrlParser: true , useUnifiedTopology: true})
    .then(() => console.log('connected to mongoDB'))
    .catch(err => console.error('not conncted ...' ,err))



//Middleware
app.use(express.json())


//routes midelwares
app.use('/api/user',authRoute)
app.use('/api/post',postRoute)


app.listen(3000, () => console.log("salam alikoum server up and runing!"))